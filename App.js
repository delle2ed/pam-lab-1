import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Linking,
  TextInput,
  NativeModules,
} from 'react-native';
import Camera from 'react-native-camera';
import Notification from 'react-native-push-notification';

const CameraManager = NativeModules.CameraManager || NativeModules.CameraModule;
const CAMERA_REF = 'camera';

var PushNotification = require('react-native-push-notification');

PushNotification.configure({

  onRegister: function (token) {
    console.log('TOKEN:', token);
  },

  onNotification: function (notification) {
    console.log('NOTIFICATION:', notification);
  },

  permissions: {
    alert: true,
    badge: true,
    sound: true
  },

  popInitialNotification: true,

  requestPermissions: true,
});

export default class App extends Component<{}> {

  constructor(props) {
    super(props);
    this.state = {
      url: '',
      type: 'back'
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TextInput style={{ flex: 1 }} onChangeText={(url) => this.setState({ url })} value={this.state.url} placeholder="Google search" />
          <View style={{ alignSelf: "center" }}>
            <Button style={{ flex: 1 }}
              title="Search" onPress={() => { Linking.openURL('https://www.google.com/search?safe=off&q=' + this.state.url) }}
            />
          </View>
        </View>
        <View style={styles.content}>
          <Camera
            ref={(cam) => {
              this.camera = cam;
            }}
            style={styles.content}
            aspect={Camera.constants.Aspect.fill}
            type={this.state.type}>
            <View style={{ flex: 1, flexDirection: 'row', }}>
              <Text style={styles.swap} onPress={() => { if (this.state.type==='front') {this.setState({type :'back'})}  else  {this.setState({type :'front'})}    }}></Text>
              <View style={{ alignSelf: "center" }}>
              </View>
              <View style={{ alignSelf: "center" }}>
                <Button style={{ alignSelf: "center" }}
                  title="Notify" onPress={() => {
                    PushNotification.localNotificationSchedule({
                      largeIcon: "ic_launcher",
                      message: "Livin la vida loca",
                      date: new Date(Date.now() + (10 * 1000))
                    });
                  }}
                />
              </View>
            </View>
            <Text style={{ flex: 4 }} ></Text>
            <Text style={styles.capture} onPress={this.takePicture.bind(this)}></Text>
          </Camera>
        </View>
      </View>
    );

  }
  takePicture() {
    const options = {};
    //options.location = ...
    this.camera.capture({ metadata: options })
      .then((data) => console.log(data))
      .catch(err => console.error(err));
  }

  changeState() {
    if (Camera.constants.type == 'back') { Camera.constants.type == 'front' }
    else { Camera.constants.type = 'back' }
    return (Camera.constants.type)
  }

  changeState2() {
    Camera.constants.type = 'front';
    // var value;
    // if (value == 'front') { this.test = 'back' }
    // else { this.test = 'front' }
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#CD5C5C'
  },

  header: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#5f9ea0',
  },

  content: {
    flex: 8
  },

  capture: {
    // backgroundColor: "#00c",
    flex: 3,
    borderRadius: 0,
    padding: 0,
    margin: 0,
  },

  swap: {
    // backgroundColor: "#cc0",
    flex: 2,
    borderRadius: 0,
    padding: 0,
    margin: 0,
  }
});